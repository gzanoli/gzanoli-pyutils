from setuptools import setup

# sys.path.insert(0, os.path.abspath('../../src'))
# import gpyutils
# from gpyutils import *


setup(
    name='gpyutils',
    version='0.3.1',
    packages=[
        'gpyutils',
        'gpyutils.datastructures',
        'gpyutils.algorithms'
    ],
    install_requires=[
        'numpy',
        'numba',
    ],
    author='Giordano Zanoli',
    author_mail='giordano.zanoli84@gmail.com',
    url="https://gitlab.com/gzanoli/gzanoli-pyutils"
)
