=========
Log Utils
=========

.. currentmodule:: gpyutils.logutils

Module Table Of Contents
------------------------
.. autosummary::
    :toctree:
    
    Elapsed
    timeit

Module API
----------
.. autoclass:: Elapsed
    :members:

.. autofunction:: timeit

