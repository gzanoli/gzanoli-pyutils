.. gzanoli-pyutils documentation master file, created by
   sphinx-quickstart on Mon Dec  3 14:56:48 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gzanoli-pyutils's documentation!
===========================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   Introduction <intro>
   Reference API <reference/index>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
