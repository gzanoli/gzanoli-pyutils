============
Introduction
============

Install
*******

As this project it is not part of the PyPI, and is only hosted on a gitlab 
repo, then you can install with pip directly from terminal typing::

    $ pip install git+https://gitlab.com/gzanoli/gzanoli-pyutils.git

To install a specific version use::

    $ pip install git+https://gitlab.com/gzanoli/gzanoli-pyutils.git@v0.1.0

where `v1.0` is the version tag in the repo. A commit sha can also be used.

Versioning
**********

Release versioning is done by means of commit tagging. 

Requirements
************

There are no other dependencies than the documentation build ones:

- sphinx
- sphinx_rtd_theme

When used in other projects, you can put in the **requirements.txt** file this 
way (non editable mode)::

    ...
    git+https://gitlab.com/gzanoli/gzanoli-pyutils.git
    ...

It may be the case that a particular version of the package should be 
installed. In this case, specify the commit sha (or tag) as follows::

    ...
    git+https://gitlab.com/gzanoli/gzanoli-pyutils.git@v0.1.0
    ...

.. warning:: Notice that the above will not install the package in editable 
    mode. If you want to install in editable mode, need to add the flag `-e` in 
    front and add the `#egg=gpyutils` at the end of the strings. This, will 
    download the package in the current directory your prompt is, henceforth 
    need also to `cd` into the env you want to install the packages (or a any 
    other desired directory), then call the `pip install -r path/to/requirements.txt`