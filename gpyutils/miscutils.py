
from copy import deepcopy
import numpy as np

__all__ = ['Backup', 'LabelEncoder']


class Backup(object):
    """
    Backup some data allowing for a future retrieval. It makes a deep copy
    of the original data, and when invoked gives back a new deep copy of 
    it. This allows to avoid running whole code that produced such data. 
    This maybe seen the in memory version of the pickle module. 

    Parameters
    ----------
    data : PyObject
        data to be stored in the Backup instance

    Examples
    --------
    Create a new Backup of some data. 

    >>> data = pd.DataFrame(np.random.randn(10,10), columns=range(10))
    >>> backup = Backup(data)
    
    Then, accidentally override, or make some undesired changes in place

    >>> data = None
    >>> data
    None
    
    To retrieve original data just call the backup

    >>> data = backup.getNew()
    >>> type(data)
    pandas.core.frame.DataFrame

    """ 
    
    def __init__(self, data):
        self.__orig = None
        self.reset(data)
    
    def getNew(self):
        """
        Returns a new deep copy of the backed up data
        """
        return deepcopy(self.__orig)
    
    def reset(self, data):
        """
        Stores new data into the instance. Changes the hidden pointer to the 
        new data, allowing the earlier deep copied data to be available for 
        garbage collector since the deep copied data only have one pointer at 
        time.
        """
        self.__orig = deepcopy(data)


class LabelEncoder(object):
    """Holds mapping class->int for a give mapping. Mimics the sklearn
    equivalent, but this object handles user defined mappings that arise in
    real world situations.

    Parameters
    ----------
    mapping : dict, optional
        mapping key to value, default is None

    Methods
    -------
    fit
    transform
    inverse_transform

    Examples
    --------
    >>> mapping = {'pluto':1, 'pippo':0}
    >>> le = LabelEncoder(mapping)
    >>> le.classes_
    ['pluto', 'pippo']
    >>> le.inverse_transform([0, 1])
    array(['pippo', 'pluto'], dtype='<U5')
    >>> le.transform(['pippo', 'pluto'])
    array([0, 1])
    >>> le.inverse_transform(le.transform(['pippo', 'pluto']))
    array(['pippo', 'pluto'], dtype='<U5')

    When the mapping is known not at instantiation time, we can use the fit
    method to restore the encoding:

    >>> mapping['paperino'] = 100
    >>> le.fit(mapping)
    >>> le.classes_
    ['paperino', 'pippo', 'pluto']
    >>> le.inverse_transform(le.transform(['pippo', 'pluto', 'paperino']))
    array(['pippo', 'pluto', 'paperino'], dtype='<U8')
    >>> for pair in le:
    ...     print(pair)
    ('paperino', 0)
    ('pippo', 1)
    ('pluto', 2)

    The fit method works the same as the sklearn analogous, it indeed fits the
    list of labels to a new encoding! This behavior is desired to match the
    sklearn API.

    """

    def __init__(self, mapping=None):
        """
        """
        self.__idx = 0

        if mapping is not None:
            self._mapping = mapping
            self._inverse_mapping = {v: k for k, v in mapping.items()}
        else:
            self._mapping = {}
            self._inverse_mapping = {}

        self._pairs = list(self._mapping.items())

    @property
    def classes_(self):
        return list(self._mapping.keys())

    def __len__(self):
        return len(self.classes_)

    def __iter__(self):
        return self

    def __next__(self):

        if not self._mapping:
            return None

        if self.__idx < len(self):
            ret = self._pairs[self.__idx]
            self.__idx += 1
            return ret
        else:
            self.__idx = 0
            raise StopIteration

    def __getitem__(self, item):
        return self._mapping[item]

    def fit(self, keys):
        """Fits a new set of keys in the LabelEncoder instance.

        .. warning:: The old mappings will be overridden when calling this
            method

        Parameters
        ----------
        keys : iterable
            of the new keys of the mapping

        Examples
        --------
        >>> mapping = {'pluto':1, 'pippo':0}
        >>> le = LabelEncoder(mapping)
        >>> le.classes_
        ['pluto', 'pippo']
        >>> le.transform(['pippo', 'pluto'])
        array([0, 1])

        The fit method called on the old instance override the old values:

        >>> mapping['paperino'] = 100
        >>> le.fit(mapping)
        >>> le.classes_
        ['paperino', 'pippo', 'pluto']
        >>> le.transform(['pippo', 'pluto', 'paperino'])
        array([1, 2, 0])

        In particular the order created in the encodings is the order of the
        mapping fitted.

        """
        self._mapping = None
        self._inverse_mapping = None
        self._pairs = None
        self._mapping = {k: i for i, k in enumerate(sorted(set(keys)))}
        self._inverse_mapping = {v: k for k, v in self._mapping.items()}
        self._pairs = list(self._mapping.items())

    def transform(self, iterable):
        """Transforms keys in iterable in the stored encoding

        Parameters
        ----------
        iterable : iterable
            the values to be encoded

        Returns
        -------
        transformed : np.array
            of encoded keys
        """
        ret = list(map(lambda x: self._mapping[x], iterable))
        return np.array(ret)

    def inverse_transform(self, iterable):
        """Transforms encodings in iterable in the corresponding class label

        Parameters
        ----------
        iterable : iterable
            the encodings to be translated

        Returns
        -------
        transformed : np.array
            of class labels
        """
        ret = list(map(lambda x: self._inverse_mapping[x], iterable))
        return np.array(ret)
