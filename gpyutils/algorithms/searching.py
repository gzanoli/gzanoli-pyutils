
import numba as nb

@nb.autojit()
def BinarySearch(array, a, index=False):
    """
    Perform binary search algorithm on the ``array`` looking for item ``a``.

    Order of growth is about :math:`N^2`. Sorting requires :math:`N^2` ops, 
    searching using binary search is order :math:`log_2 N`, then negligible: 
    most of the time is spent in sorting algo.

    Args
        array : an iterable of comparable elements
        a : an element comparable with elements in ``array``
        index : bool, if True the it returns the location of the searched 
            ``key`` in the ``array``. If not found, returns -1
    
    Return
        bool or int, if ``a`` is present in the ``array``, or the location
    """
    array = sorted(array)
    lo, hi = 0, len(array) - 1
    while lo <= hi:
        mid = lo + (hi-lo) // 2
        if   a < array[mid]: hi = mid - 1
        elif a > array[mid]: lo = mid + 1
        else: 
            if index: return mid
            else: return True # lo == hi
    if index: return -1
    else: return False # lo > hi


# debug
if __name__ == '__main__':
    
    import numpy as np
    arr = np.random.randint(0,100,100)
    a = 50
    print(BinarySearch(arr, a))
