import numba as nb
import numpy as np
from abc import ABCMeta, abstractmethod


__all__ = ['Selection','Insertion','Shell','Merge','BUMerge','Quick',
           'MedianOfThree','TukeysNinther','test']


class Sort(object, metaclass=ABCMeta):

    @abstractmethod
    def sort():
        pass


class Selection(Sort):
    """
    Implements selection sort algorithm. This algorithm is the trivial approach
    in solving the sorting problem. It takes about :math:`~N^2/2` compares and
    :math:`N` swaps. It works in place therefore it doesn't takes too much
    memory other than the already used.

    It doesn't depend on the input. An already sorted array will take the same
    amount of time to be sorted.

    This method doesn't preserve ordering, it is **not stable**.

    References
    ----------
    ..[1] R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson
    Education*

    Notes
    -----
    Also known as *Bubble-sort** algorithm

    """
    @nb.autojit
    def sort(array):
        """
        Parameters
        ----------
        array : np.ndarray or iterable

        Returns
        -------
        sorted
        """
        n = len(array)
        for i in range(n):
            for j in range(i+1, n):
                if array[i] > array[j]:
                    array[i], array[j] = array[j], array[i]


class Insertion(Sort):
    """
    Implements insertion sort algorithm. This algorithm is an improvement of
    ``Selection.sort`` algorithm. It takes in average :math:`~N^2/4` compares
    and :math:`~N^2/4` swaps. It works in place therefore it doesn't takes too .
    much memory other than the already used.

    It is dependent upon the input array structure. If the array is already
    sorted, then it is linear in time and only computes :math:`N-1` compares.
    If it is in the reverse order, then it will do :math:`N^2/2` compares and
    :math:`N^2/2` swaps.

    This method does preserve ordering, it is **stable**.

    References
    ----------
    ..[1] R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson
    Education*

    """
    # def sort(array):
    #     n = len(array)
    #     for i in range(1, n):
    #         j = i
    #         while j > 0 and array[j-1] > array[j]:
    #             array[j], array[j-1] = array[j-1], array[j] # two changes per loop, 4 access
    #             j -= 1
    @nb.autojit
    def sort(array):
        """
        Parameters
        ----------
        array : np.ndarray or iterable

        Returns
        -------
        sorted
        """
        # this version has much less array access. Use the guaranteed fact
        # that elements on the left of the current index are already sorted
        n = len(array)
        for i in range(1, n):
            ival = array[i]
            j = i
            while j > 0 and array[j-1] > ival:
                # this strictly greater guarantees stability
                array[j] = array[j-1] # only one change per loop, 2 array access
                j -= 1
            array[j] = ival


class Shell(Sort):
    """
    Implements shell sort algorithm. This algorithm is uses the techniques of
    ``Insertion.sort`` algorithm. It takes in average :math:`~N^{6/5}`
    compares. However, this is just from experience, no mathematical model has
    been discovered so far for description of running time and order of growth.
    Furthermore, performance heavily depends on the *h*-value that determines
    the *h*-sorted subsequences in the array. This algorithm uses the
    *increment sequence* :math:`h \\leftarray 3h+1` achieving the above order
    of growth. Higher performances might be achieved using better
    *increment sequences*.

    This method provide a very good choice in terms of algorithm complexity and
    performance. Further, this should be the reference choice if extraspace is
    a problem as fastest algos do require extra room to store intermediate
    steps of the calculations.

    This method doesn't preserve ordering, it is **not stable**.

    References
    ----------
    ..[1] R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson
    Education*

    """
    @nb.autojit
    def sort(array):
        """
        Parameters
        ----------
        array : np.ndarray or iterable

        Returns
        -------
        sorted

        Timings
        -------
        It uses numba autojit facilities, therefore at first instance might be
        slower. Comparing with inplace sort of numpy arrays:

        >>> from .logutils import Elapsed
        >>> tbs = np.random.randn(1000000)
        >>> et = Elapsed()
        >>> tbs.sort()
        >>> print(et.printFromStart())
        Elapsed Time: 0.077495 [secs]
        >>> tbs = list(np.random.randn(1000000))
        >>> et = Elapsed()
        >>> Shell.sort(tbs)
        >>> print(et.printFromStart())
        Elapsed Time: 0.268997 [secs]
        >>> tbs = np.random.randn(1000000)
        >>> et = Elapsed()
        >>> Shell.sort(tbs)
        >>> print(et.printFromStart())
        Elapsed Time: 0.188999 [secs]

        In order to check goodness of algorithm:

        >>> import matplotlib.pyplot as plt
        >>> plt.plot(tbs)

        """
        n = len(array)
        # set h, increment sequence
        h = 1
        while h <= n//3:
            h = 3*h+1
        # shell sort
        while h >= 1:
            for i in range(h, n):
                j = i
                # while j >= h and array[j-h] > array[j]:
                #     array[j], array[j-h] = array[j-h], array[j]
                jVal = array[j]
                while j >= h and array[j-h] > jVal:
                    array[j] = array[j-h]
                    # only one change per loop, 2 array access
                    j -= h
                array[j] = jVal
            h = h // 3


@nb.autojit
def merge(array,aux,lo,mid,hi):
    """
    Merges to disjoint sorted arrays stored into the ``array``
    """
    i = lo
    j = mid+1

    for k in range(lo,hi+1):
        if i > mid: # we are done with left array
            aux[k] = array[j]
            j += 1
        elif j > hi: # we are done with right array
            aux[k] = array[i]
            i += 1
        elif array[j] < array[i]:
            aux[k] = array[j]
            j += 1
        else:
            aux[k] = array[i]
            i += 1

@nb.autojit
def _td_merge_sort(array,aux,lo,hi,CUTOFF=7):
    # if hi <= lo + CUTOFF -1:
    #     _insertion_sort_for_small_subarrays(aux,lo,hi)
    #     return
    if hi <= lo:
        return
    mid = (lo + hi) // 2
    _td_merge_sort(aux,array,lo,mid)
    _td_merge_sort(aux,array,mid+1,hi)
    merge(array,aux,lo,mid,hi)


class Merge(Sort):
    """
    Implements top-down mergesort algorithm. It is guaranteed to take at most
    :math:`~N \\log(N)` compares and at most :math:`6N\\log(N)` array accesses.
    It doens't work in place therefore it shouldn't be used when memory space
    is an issue. It uses additional memory proportional to :math:`N`.
    Furthermore, since it uses a recursive strategy (number of recursions
    :math:`~\\log(N)`), also the in-memory local environments should be
    considered.

    It is dependent upon the input array structure. If it is already ordered
    it runs in :math:`~\\log(N)` time.

    This method does preserve ordering, it is **stable**.

    References
    ----------
    ..[1] R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson
    Education*

    """

    @nb.autojit
    def sort(array):
        """
        Parameters
        ----------
        array : np.ndarray or iterable

        Returns
        -------
        sorted

        Timings
        -------
        It uses numba autojit facilities, therefore at first instance might be
        slower. Comparing with inplace sort of numpy arrays:

        >>> from .algorithms.sorting import Merge, test
        >>> test(Merge,1000000)
        Elapsed Time: 0.145002 [secs]
        >>> test('python',1000000)
        Elapsed Time: 0.078007 [secs]
        
        """
        aux = array.copy()
        _td_merge_sort(aux,array,0,len(array)-1)


@nb.autojit
def _bu_merge(array,aux,lo,mid,hi):
    """
    Merges to disjoint sorted arrays stored into the ``array``
    """
    i = lo
    j = mid+1

    for k in range(lo,hi+1):
        if i > mid: # we are done with left array
            array[k] = aux[j]
            j += 1
        elif j > hi: # we are done with right array
            array[k] = aux[i]
            i += 1
        elif aux[j] < aux[i]:
            array[k] = aux[j]
            j += 1
        else:
            array[k] = aux[i]
            i += 1


class BUMerge(Sort):
    """
    Implements bottom-up mergesort algorithm. It is guaranteed to take at most
    :math:`~N \\log(N)` compares and at most :math:`6N\\log(N)` array accesses.
    It doens't work in place therefore it shouldn't be used when memory space
    is an issue. It uses additional memory proportional to :math:`N`.
    Furthermore, since it uses a recursive strategy (number of recursions
    :math:`~\\log(N)`), also the in-memory local environments should be
    considered.

    It is dependent upon the input array structure. If it is already ordered
    it runs in :math:`~\\log(N)` time.

    This method does preserve ordering, it is **stable**. Should be used when 
    stability is a requirement. When it is not, and performance is the key 
    feature to optimize jointly with space, use ``Quick.sort`` instead.

    References
    ----------
    ..[1] R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson
    Education*

    See Also
    --------
    :meth:`Quick.sort`

    """
    @nb.autojit
    def sort(array):
        aux = array.copy()
        N = len(array)
        sz = 1
        while sz < N:
            lo = 0
            while lo < N-sz:
                mid = lo+sz-1
                hi = min(lo+sz+sz-1, N-1)
                _bu_merge(array, aux, lo, mid, hi)
                lo += sz+sz
            sz += sz
            aux = array.copy()


@nb.autojit
def _insertion_sort_for_small_subarrays(array,lo,hi):
    # this version has much less array access. Use the guaranteed fact
    # that elements on the left of the current index are already sorted
    for i in range(lo+1, hi+1):
        ival = array[i]
        j = i
        while j > 0 and array[j-1] > ival:
            # this strictly greater guarantees stability
            array[j] = array[j-1] # only one change per loop, 2 array access
            j -= 1
        array[j] = ival


@nb.autojit
def partition(a, lo, hi):
    i = lo + 1
    j = hi
    pivot = a[lo]
    
    while True:
        # from left
        while a[i] < pivot:
            i += 1
            # if i == hi: break redundant as sentinel used at end of the array
        # from right
        while pivot < a[j]:
            j -= 1
            # if j == lo: break redundant, can never happen since i>=j
        # swap
        if i >= j: break
        a[i], a[j] = a[j], a[i]
    # final swap of the partition item
    a[lo], a[j] = a[j], a[lo]
    return j


@nb.autojit
def _quick_sort(array, lo, hi, CUTOFF=10):
    if hi <= lo + CUTOFF - 1:
        _insertion_sort_for_small_subarrays(array,lo,hi)
        return
    j = partition(array,lo,hi)
    _quick_sort(array,lo,j-1)
    _quick_sort(array,j+1,hi)


@nb.autojit
def MedianOfThree(array,a,b,c):
    """
    Parameters
    ----------
    array : np.ndarray or iterable
        of comparables
    
    a, b, c : int
        indices of elements to be checked, in the range of len(array)

    Returns
    -------
    The index of the median element of the three
    """
    if array[a] < array[b]:
        if array[a] < array[c]: # a is the minimum of 3
            return min(b,c) # a < b,c
        else: # c < a < b 
            return a
    else: # a > b
        if array[a] > array[c]: # a is the maximum of 3
            return max(b,c) # a > b,c
        else: # b < a < c 
            return a

@nb.autojit
def TukeysNinther(a,lo,hi):
    """
    Finds the *Tukey's Ninther* [1]_ of an array ``a`` in the range among the 
    indices ``i`` and ``j`` 

    Parameters
    ----------
    a : np.ndarray or itereable

    lo, hi : int
        in the range len(a)

    Returns
    -------
    The median sampled element using the *Tukey's Ninther* strategy, and swap 
    in place a[lo] with such an element

    References
    ----------
    ..[1] R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson
    Education*

    """
    step = 2
    if hi - lo + 1 >= 9*step:
        m = [0,0,0]
        for i in range(3):
            base = lo + i*6
            i1 = base + 0
            i2 = base + 2
            i3 = base + 4
            m[i] = MedianOfThree(a,i1,i2,i3)
        return MedianOfThree(a, m[0],m[1],m[2])
    return lo


@nb.autojit
def _3way_quick_sort(a,lo,hi,CUTOFF=10):
    if hi <= lo + CUTOFF - 1:
        _insertion_sort_for_small_subarrays(a,lo,hi)
        return
    i  = lo
    lt = lo
    gt = hi
    # tn = TukeysNinther(a,lo,hi) # add Tukey's ninther here
    # a[lo], a[tn] = a[tn], a[lo]
    v = a[lo]
    while i <= gt:
        ai = a[i]
        if ai < v:
            a[lt], a[i] = a[i], a[lt]
            i  += 1
            lt += 1
        elif ai > v:
            a[gt], a[i] = a[i], a[gt]
            gt -= 1
        else:
            i  += 1
    _3way_quick_sort(a, lo, lt-1)
    _3way_quick_sort(a, gt+1, hi)


# @nb.autojit
# def _3way_quick_sort_faster(a,lo,hi,CUTOFF=10):
#     """
#     References
#     ----------
#     [1] J. Bentley, D. McIlroy - **Engineering a sort function** (1991)
#     """
#     if hi <= lo + CUTOFF - 1:
#         _insertion_sort_for_small_subarrays(a,lo,hi)
#         return
#     i  = lo # 0
#     p  = lo # 0
#     j  = hi # n-1
#     q  = hi # n-1
#     n  = hi
#     v = a[lo] # add Tukey's ninther here
#     while True:
#         ai = a[i]
#         while i <= j and ai <= v:
#             if ai == v:
#                 a[p], a[i] = a[i], a[p]
#                 p += 1
#             i += 1
#         aj = a[j]
#         while i <= j and aj >= v:
#             if aj == v:
#                 a[q], a[j] = a[j], a[q]
#                 q -= 1
#             j -= 1
#         if i > j:
#             break
#         a[i], a[j] = a[j], a[i]
#         i += 1
#         j -= 1
    
#     # final swap equals from side to center
#     s = min(p, i-p)
#     h = i - s
#     for l in range(s):
#         a[l], a[h] = a[h], a[l]
#         h += 1
#         l += 1
#         s -= 1
#     s = min(n-1-q, j-q)
#     h = n - s
#     for l in range(i, s):
#         a[l], a[h] = a[h], a[l]
#         h += 1
#         l += 1
#         s -= 1

#     _3way_quick_sort_faster(a, lo, lt-1)
#     _3way_quick_sort_faster(a, gt+1, hi)

@nb.autojit
def _select(array, k):
    np.random.shuffle(array)
    lo = 0
    hi = len(array)-1
    while hi > lo:
        j = partition(array, lo, hi)
        if j == k:
            return array[k]
        elif j > k:
            hi = j-1
        elif j < k:
            lo = j+1
    return array[k]

class Quick(Sort):
    """
    Implements quicksort algorithm. It is guaranteed (in probability) to take 
    at most :math:`~1.39 N \\log(N)` compares (twice more than mergsort) but 
    much less array accesses (againg, in probability). Furthermore, it works in 
    place, and this make it faster than mergesort. The algorithm uses the 3-way 
    partitioning [2]_, slightly slower but guarantees to mantain the order of 
    growth under duplicate keys.

    Shuffling version, guaratees (in probability, for large arrays) that an
    already sorted array runs in :math:`~N\\log(N)` time, rather than 
    :math:`~N^2` of the non shuffled version.

    This method does not preserve ordering, it is **not stable**. Should be 
    used when runtime is the main worry and stability is not important, for 
    example with primitive types. For custom objects, usually stability is a 
    desirable property, whence use ``Merge.sort`` instead, in such a case.

    References
    ----------
    ..[1] R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson
    Education*
    
    ..[2] E. W. Dijkstra - **A Discipline of Programming** 1976 - *Prentice 
    Hall*

    See Also
    --------
    :meth:`Merge.sort`

    """
    @nb.autojit
    def sort(array,CUTOFF=10,duplicate_keys=True):
        """
        Parameters
        ----------
        array : np.ndarray or iterable
            to be sorted
        
        CUTOFF : int, 10
            parameter tuning for speedness
        
        duplicate_keys : bool, True
            if True the 3-way Quicksort algorithm is used, that guarantees
            against runtime blow up in case of duplicate keys. Otherwise, 
            the faster basic implementation is used

        Returns
        -------
        sorted

        Timings
        -------
        It uses numba autojit facilities, therefore at first instance might be
        slower. Comparing with inplace sort of numpy arrays:

            >>> from .algorithms.sorting import Quick, test
            >>> test(Quick,1000000,duplicate_keys=False)
            Elapsed Time: 0.123998 [secs]
            >>> test('python',1000000)
            Elapsed Time: 0.078007 [secs]

        Compared to the built-in ``sorted`` function:

            >>> a = np.random.randn(1000000)
            >>> et = Elapsed()
            >>> sorted(a)
            >>> print(et.printFromBreakpoint())  
            Elapsed Time: 0.775996 [secs]  

        """
        n = len(array) - 1
        np.random.shuffle(array)
        argmax = np.argmax(array)
        array[argmax], array[n] = array[n], array[argmax]
        if duplicate_keys:
            _3way_quick_sort(array,0,n,CUTOFF)
        else:
            _quick_sort(array,0,n,CUTOFF)


    @nb.autojit
    def select(array, k):
        """
        In an unordered array, select the k-th smallest element.

        Parameters
        ----------
        array : np.ndarray or iterable
            to be sorted
        
        k : int
            the index of the k smallest element to search for
        
        Returns
        -------
        the k-th smallest element of the ``array``.

        References
        ----------
        ..[1] R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson
        Education*
        
        """
        return _select(array, k)

    @nb.autojit
    def median(array):
        """
        In an unordered array, select the median element, i.e. the k/2-th 
        element (integer division).

        Parameters
        ----------
        array : np.ndarray or iterable
            to be sorted
        
        Returns
        -------
        the median element of the ``array``.

        References
        ----------
        ..[1] R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson
        Education*
        
        """        
        k = len(array) // 2
        return _select(array, k)


def test(algo, n=10, **kwargs):
    import numpy as np
    import matplotlib.pyplot as plt
    from ..logutils import Elapsed
    tbs = np.random.randn(n)
    et = Elapsed()
    if algo=='python':
        tbs.sort()
    else:
        algo.sort(tbs, **kwargs)
    print(et.printFromStart())
    plt.plot(tbs)
    plt.show()
    plt.close()


def testCutOff(algo,n,cut_off):
    import numpy as np
    from ..logutils import Elapsed
    tbs = np.random.randn(n)
    et = Elapsed()
    algo.sort(tbs,cut_off)
    return et.current_runtime
    

if __name__ == '__main__':

    tbs = np.random.randn(20)
    Quick.sort(tbs)

    tbs = np.random.randn(20)
    TukeysNinther(tbs,0,len(tbs)-1)