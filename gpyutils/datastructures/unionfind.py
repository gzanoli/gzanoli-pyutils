
from abc import ABCMeta, abstractmethod

import numpy as np
import numba as nb

__all__ = ['UFQuickFind', 'UFQuickUnion', 'UFWeightedQuickUnion']

@nb.autojit
def nbEagerUnion(array, p, q):
    pid = array[p]
    qid = array[q]

    if pid != qid: 
        for i, iid in enumerate(array):
            if iid == pid:
                array[i] = qid
    
    return array

@nb.autojit
def nbWeightedQuickUnionFind(array, p):
    """
    Component identifier for ``p`` (0 to N-1), that is the id of the root. 
    Max order :math:`2N`
    """
    visited_nodes = []
    while p != array[p]:
        visited_nodes.append(p)
        p = array[p]
    for n in visited_nodes:
        array[n] = p
    return array, p


@nb.autojit
def nbWeightedQuickUnion(array, size_array, p, q, count):
    """
    Max order is twice :math:`log_2 N`.
    """
    array, i = nbWeightedQuickUnionFind(array, p)
    array, j = nbWeightedQuickUnionFind(array, q)

    if i != j: 
        if size_array[i] < size_array[j]:
            size_array[i] = j
            size_array[j] += size_array[i]
        else: # self._sz[i] >= self._sz[j]:
            size_array[j] = i
            size_array[i] += size_array[j]

        count -= 1
    
    return array, size_array, count


class UnionFind(object, metaclass=ABCMeta):
    """
    Given a collection of :math:`N` objects indexed from :math:`0,...,N`, 
    try to determine if:
        
    - two components ``p`` and ``q`` are connected
    - the number of sets of connected components

    This data structure study the connectivity just by addressing the problem 
    of ``p`` and ``q`` connected or not. The set of connections that links 
    that pair is a different problem studied in a **graph** data structure.

    Parameters
    ----------
    n : integer, default is 0
        represent the number of objects to be linked

    Methods
    -------
    union(p,q) : returns ``None``
        add connection among ``p`` and ``q``
    find(p) : returns ``int``
        component identifier for ``p``
    connected(p,q) : returns ``bool``
        True if ``p`` and ``q`` in the same component
    count() : returns ``int``
        The number of components
    """

    def __init__(self, n=0):
        self._id = np.arange(n)
        self._count = n
    
    @abstractmethod
    def union(self):
        pass
    
    @abstractmethod
    def find(self):
        pass
    
    def connected(self, p, q):
        """
        Return ``True`` if ``p`` and ``q`` are in the same equivalence class, 
        i.e. are in the same tree (this amounts to checking if the two 
        components share the same root)
        """
        i, j = self.find(p), self.find(q)
        return self._id[i] == self._id[j]

    @property
    def count(self):
        """
        Return the number of connected components
        """
        return self._count


class UFQuickFind(UnionFind):
    __doc__ = UnionFind.__doc__
    __doc__ += """
    The underlying data structure is an array where each index represent each
    object and the value is the component identifier.

    Two elements are in the same equivalence class iff their ``self._id[]`` are 
    equal. The ``UFQuickFind`` data structure is so called since
    the ``find`` method has a constant time access. However, this strategy is 
    not useful for large datasets as the ``union`` conversely requires 
    :math:`N+3` to :math:`2N+1` array access, so grows linearly and doesn't 
    scale. Further, the ``component`` method requires 2 array accesses. [1]_

    References
    ----------
    [1]_ R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson 
    Education*
    """
    
    def __init__(self, n=0):
        UnionFind.__init__(self, n)

    def union(self, p, q):
        """
        Eager approach for adding connection
        """
        pid = self._id[p]
        qid = self._id[q]

        if pid != qid: 
            for i, iid in enumerate(self._id):
                if iid == pid:
                    self._id[i] = qid

            self._count -= 1
    
    def find(self, p):
        """
        Component identifier for ``p`` (0 to N-1). Order N
        """
        return self._id[p]
    

class UFQuickUnion(UnionFind):
    __doc__ = UnionFind.__doc__
    __doc__ += """
    The underlying data structure is an array where each index represent each
    object and the value is a link to the parent. This results in a forest of 
    trees represented as parent-link

    Two elements are in the same equivalence class iff their ``self._id[]`` are 
    equal. The ``UFQuickUnion`` data structure is so called since
    the ``quick`` method has a constant time access. However, it calls twice 
    the ``find`` method, which conversely requires up to :math:`N` array 
    accesses, so it may grow linearly and may not scale. This largely depends 
    on the underlying data and it is not predictable. However, this is the 
    worst case (height of the tree equal to :math:`N`, just one single 
    component), therfore is generally faster than the ``UFQuickFind`` 
    implementation. 
    
    Finally, the ``component`` method requires 2 array accesses. [1]_

    References
    ----------
    [1]_ R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson 
    Education*
    """
    
    def __init__(self, n=0):
        UnionFind.__init__(self, n)

    def union(self, p, q):
        """
        Max order is twice :math:`N` plus 1.
        """
        i = self.find(p)
        j = self.find(q)

        if i != j: 
            self._id[i] = j
            self._count -= 1
    
    def find(self, p):
        """
        Component identifier for ``p`` (0 to N-1), that is the id of the root. 
        Max order :math:`N`
        """
        while p != self._id[p]:
            p = self._id[p]
        return p
    

class UFWeightedQuickUnion(UnionFind):
    __doc__ = UnionFind.__doc__
    __doc__ += """
    The underlying data structure is an array where each index represent each
    object and the value is a link to the parent. This results in a forest of 
    trees represented as parent-link.

    Two elements are in the same equivalence class iff their ``self._id[]`` are 
    equal. The ``UFWeightedQuickUnion`` is a modification of the 
    ``UFQuickUnion`` that allows the union operation to grow in :math:`log_2 N`
    order. This is acheived by clever choice in the ``union`` operation. Rather 
    than arbitrary choice, we always link the tree of smaller size to the root 
    of the bigger one. The key here is to observe that a tree of *size* 
    :math:`k` has *height* of at most :math:`log_2 k`.
    
    The price to pay is the memory usage of a second array
    that for each component stores the current tree size. 

    This also uses the *path compression* strategy in the ``find`` operation, 
    so that the array accesses grows in almost constant time.
    
    Finally, the ``component`` method requires 2 array accesses. [1]_

    References
    ----------
    [1]_ R. Segdwick, K. Wayne - **Algorithms 4th Edition** 2011 - *Pearson 
    Education*
    """
    def __init__(self, n=0):
        UnionFind.__init__(self, n)
        self._sz = np.ones(n)

    def union(self, p, q):
        """
        Max order is twice :math:`log_2 N`.
        """
        i = self.find(p)
        j = self.find(q)

        if i != j: 
            if self._sz[i] < self._sz[j]:
                self._id[i] = j
                self._sz[j] += self._sz[i]
            else: # self._sz[i] >= self._sz[j]:
                self._id[j] = i
                self._sz[i] += self._sz[j]

            self._count -= 1

    def find(self, p):
        """
        Component identifier for ``p`` (0 to N-1), that is the id of the root. 
        Max order :math:`2N`
        """
        visited_nodes = []
        while p != self._id[p]:
            visited_nodes.append(p)
            p = self._id[p]
        for n in visited_nodes:
            self._id[n] = p
        return p


if __name__ == '__main__':
    
    N = 25
    uf = UFWeightedQuickUnion(N)
    connections = list(zip(np.random.permutation(N), np.random.permutation(N)))

    for p,q in connections:
        uf.union(p,q)
    


